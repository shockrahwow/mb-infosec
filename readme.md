# What is this?

Here you'll find a few resources to start learning about reverse enginerring and hacking by doing, not by reading.
For now the binary subdirectory will be focued mainly on x86 assembly but for transparency reasons the source code for each binary will be provided. 

_Just know that right now there isn't much content here yet but there will be more as time goes on._

## bin/

Contains 64-bit ELF-binaries meant for reverse engineering.
Each binary should be standalone but if theres a sub-directory then it means there's a challenge in that sub-directory alone.

## obfus/ 

Source code challenges similar to the binary reversing challenges but with source code in mind.
Each challenge may be compiled if you wish but do use the `make.sh` script to build each file as it contains the proper compiler flags for each challenge.

The make file itself is also obfuscated but not by much ;).

## Building 

Compilation for any given challenge will use a command like this: `sh make.sh src/example.c output-path`
